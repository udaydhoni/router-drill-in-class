import React from "react";
import { NavLink } from "react-router-dom";
class Home extends React.Component {
    constructor(props) {
        super()
    }
    render () {
        return (
            <div className="home-page">
                <h2>🚀 Welcome to Homepage!</h2>
                <NavLink to ='/articles' className="article-btn-link"><p className="article-btn">Articles Page</p></NavLink>
            </div>
            
        )
    }
}

export default Home