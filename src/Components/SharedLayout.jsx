import {Link, NavLink} from 'react-router-dom';
import './SharedLayout.css'
function SharedLayout () {
    return (
        <div>
            <div className="header">
                <NavLink to='/' className='header-link'>
                    <p className="header">Dashboard</p>
                </NavLink>
                
            </div>
            <div className="sidebar">
                <NavLink to = '/'   className = 'home' style={({isActive})=>{return {backgroundColor: isActive ? 'rgb(207, 202, 202)':'white'}}}>
                    <section>🏠Home</section>
                </NavLink>
                <NavLink to = 'articles' className = 'article' style={({isActive})=>{return {backgroundColor: isActive ? 'rgb(207, 202, 202)':'white'}}}>
                    <section>📝Article</section>
                </NavLink>
            </div>
        </div>
    )
}

export default SharedLayout