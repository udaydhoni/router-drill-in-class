import React from "react"
import data from "../data"
import Item from "./Item"

class Articles extends React.Component {
    constructor (props) {
        super()
        this.state = {
            search: '',
            article: data
        }
    }
    searchHandler = (event)=>{
        let query= event.target.value
        let updatedData = data.filter((elem)=> elem['title'].toLowerCase().includes(query.toLowerCase()))
        this.setState({
            search: query,
            article: updatedData
        })
    }
    render () {
        console.log('articles rendering')
        return (
            <div >
                <input type='text' id='search' placeholder="Search" onChange={this.searchHandler} className='search'></input>
                {this.state.article.map((elem, index)=>{
                    return <Item data={elem} key={index}></Item>
                })}
            </div>
            
        )
    }
}

export default Articles