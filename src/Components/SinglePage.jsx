import React from "react";
import { NavLink } from "react-router-dom";
import data from "../data";

class SinglePage extends React.Component {
    constructor (props) {
        super ()
    }
    render () {
        return (
            <div>
                <NavLink to= '/articles'>Go back to articles</NavLink>
                <p>The slug of the article is ::: {this.props.match.params.slugger}</p>
            </div>
        )
    }
}

// function SinglePage (props) {
//     let params = useParams()
//     return (
//         <div>
//             <NavLink to= '/articles'><p className="goback">👈Go back to articles</p></NavLink>
//             <p className="goback msg">The slug of the article is ::: {params.slugger}</p>
//         </div>
//     )
// }

export default SinglePage