import React from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import SharedLayout from './Components/SharedLayout'
import Home from './Components/Home'
import { Route} from 'react-router-dom'
import Articles from './Components/Articles'
import SinglePage from './Components/SinglePage'
import Error from './Components/Error'
import data from './data'

class App extends React.Component {
  constructor(props) {
    super ()
    this.state = {

    }
  }
  render () {
    return (
          <div>
              <SharedLayout></SharedLayout>

              <Route path='/' exact><Home></Home></Route>
              <Route path='/articles' exact><Articles></Articles></Route>
              <Route path='/articles/:slugger' component={SinglePage}></Route>
              <Route path='*' element={<Error></Error>}></Route>
          </div>
          
            
        
      
    )
  }
}

export default App
